Feature: As a User I want to perform all the actions on CustomerAgreement

 	Scenario: User is performing GET action on CustomerAgreement
	    Given User has body data for GET action on CustomerAgreement
	    When User has Cookie and performs GET action on CustomerAgreement using URL
	    Then user should get CustomerAgreement_response Error_spcCode and Error_spcMessage
	    
	Scenario: User gets Error message when BCNumber is missing in URL when performing GET on CustomerAgreement
		When BCNumber is missing in URL when performing GET on CustomerAgreement
		Then User should get CustomerAgreement_response Error message for missing BCNumber
		
	Scenario: User gets Error message when BCNumber is invalid in URL when performing GET on CustomerAgreement
		When BCNumber is invalid in URL when performing GET on CustomerAgreement
		Then User should get CustomerAgreement_response Error message for invalid BCNumber