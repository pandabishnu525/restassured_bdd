# RestAssured_BDD
This is a cucumber based restAssured framework which uses the BDD language for the test scenarios.

## How the Framework functions
- Feature files contain the test scenarios.
- For each feature file there is a definition file linked.
- The Definition file has all the steps and required script corresponding to the feature files.
- The function library is used to maintain the reusable functions which are frequently used in definition files.
- The resource file contains the properties or external data which is needed for the functions.
- The runner file contains the link for the feature folder and respective difinitions.
