package employeeJourney_REST_stepDefinition;

import org.testng.Assert;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import employeeJourney_REST_Functions.FunctionLibrary;
import employeeJourney_REST_Property.ReadPropertiesFile;
import io.restassured.response.Response;

public class GetOwner_StepDefinition {

	static String body;
	static String Output;
	static String url;
	static String DossierID;
	static String Session;
	
	ReadPropertiesFile ReadPropertiesFileObj = new ReadPropertiesFile();
	
	/*Verify that user is bale to perform GET action on GETOwner*/
	
	@When("^User has Cookie and performs GET action for GETOwner using URL$")
	public void user_has_Cookie_and_performs_GET_action_for_GETOwner_using_URL() throws Throwable {
		System.out.println("Start==>> User has started GET action for GET Owner");
		FunctionLibrary PostActionObj = new FunctionLibrary();
		Response res = PostActionObj.GetResponse_GET(ReadPropertiesFileObj.getCookie(), ReadPropertiesFileObj.URL_GETOwner());
		Output = res.asString();
	}

	@Then("^User should get response for GETOwner$")
	public void user_should_get_response_for_GETOwner() throws Throwable {
		Assert.assertTrue(Output.contains(ReadPropertiesFileObj.getOwner()));
		System.out.println("User has successfully performed GET action on Get Owner");
	}
}
