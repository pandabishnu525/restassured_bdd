package employeeJourney_REST_stepDefinition;

import org.testng.Assert;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import employeeJourney_REST_Functions.FunctionLibrary;
import employeeJourney_REST_Property.ReadPropertiesFile;
import io.restassured.response.Response;

public class CustomerAgreement_allActions_StepDefinition {

	static String body;
	static String Output;
	static String url;
	static String DossierID;
	static String Session;
	
	ReadPropertiesFile ReadPropertiesFileObj = new ReadPropertiesFile();
	
	@Given("^User has body data for GET action on CustomerAgreement$")
	public void user_has_body_data_for_GET_action_on_CustomerAgreement() throws Throwable {
		FunctionLibrary PostActionObj = new FunctionLibrary();
		System.out.println("Start==>> User has started GET action for CustomerAgreement");
		url = PostActionObj.GetURL_CUSTOMER_AGREEMENT(ReadPropertiesFileObj.CustomerAgreement_BCNumber());	    
	}

	@When("^User has Cookie and performs GET action on CustomerAgreement using URL$")
	public void user_has_Cookie_and_performs_GET_action_on_CustomerAgreement_using_URL() throws Throwable {
		FunctionLibrary PostActionObj = new FunctionLibrary();
		Response res = PostActionObj.GetResponse_GET(ReadPropertiesFileObj.getCookie(), url);
		Output = res.asString();	    
	}

	@Then("^user should get CustomerAgreement_response Error_spcCode and Error_spcMessage$")
	public void user_should_get_CustomerAgreement_response_Error_spcCode_and_Error_spcMessage() throws Throwable {
		Assert.assertTrue(Output.contains(ReadPropertiesFileObj.CustomerAgreement_BCNumber()));
		System.out.println("User has successfully verified GET response on CustomerAgreement");
	}
	@When("^BCNumber is missing in URL when performing GET on CustomerAgreement$")
	public void bcnumber_is_missing_in_URL_when_performing_GET_on_CustomerAgreement() throws Throwable {
		FunctionLibrary PostActionObj = new FunctionLibrary();
		System.out.println("Start==>> User has started GET action for CustomerAgreement when BCNumber is missing in URL");
		url = PostActionObj.GetURL_CUSTOMER_AGREEMENT("");
		Response res = PostActionObj.GetResponse_GET(ReadPropertiesFileObj.getCookie(), url);
		Output = res.asString();
	}

	@Then("^User should get CustomerAgreement_response Error message for missing BCNumber$")
	public void user_should_get_CustomerAgreement_response_Error_message_for_missing_BCNumber() throws Throwable {
		Assert.assertTrue(Output.contains(ReadPropertiesFileObj.ErrorCode_Missing()));
		Assert.assertTrue(Output.contains(ReadPropertiesFileObj.ErrorMessage_Missing()));
		System.out.println("User has successfully verified GET response on CustomerAgreement when BCNumber is missing in URL");
	}

	@When("^BCNumber is invalid in URL when performing GET on CustomerAgreement$")
	public void bcnumber_is_invalid_in_URL_when_performing_GET_on_CustomerAgreement() throws Throwable {
		FunctionLibrary PostActionObj = new FunctionLibrary();
		System.out.println("Start==>> User has started GET action for CustomerAgreement when BCNumber is invalid in URL");
		url = PostActionObj.GetURL_CUSTOMER_AGREEMENT(ReadPropertiesFileObj.Invalid_AccBC());
		Response res = PostActionObj.GetResponse_GET(ReadPropertiesFileObj.getCookie(), url);
		Output = res.asString();
	}

	@Then("^User should get CustomerAgreement_response Error message for invalid BCNumber$")
	public void user_should_get_CustomerAgreement_response_Error_message_for_invalid_BCNumber() throws Throwable {
		Assert.assertTrue(Output.contains(ReadPropertiesFileObj.ErrorCode_Missing()));
		Assert.assertTrue(Output.contains(ReadPropertiesFileObj.ErrorMessage_Missing()));
		System.out.println("User has successfully verified GET response on CustomerAgreement when BCNumber is invalid in URL");
	}
}
