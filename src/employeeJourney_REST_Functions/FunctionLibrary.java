package employeeJourney_REST_Functions;

import static io.restassured.RestAssured.given;
//import java.util.Set;
//import org.openqa.selenium.Cookie;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.firefox.FirefoxDriver;
import employeeJourney_REST_Property.ReadPropertiesFile;
import io.restassured.response.Response;

public class FunctionLibrary {

	ReadPropertiesFile ReadPropertiesFileObj = new ReadPropertiesFile();
	
	public String GetURL_CUSTOMER_AGREEMENT(String CustomerAgreement_BCNumber) {		
		String GetURL_CUSTOMER_AGREEMENT = ReadPropertiesFileObj.URL_GET_PARTY_DETAIL()+CustomerAgreement_BCNumber;
		return GetURL_CUSTOMER_AGREEMENT;
	}		
	public Response GetResponse_GET(String Session , String url) {
		System.setProperty("http.proxyHost", "10.240.51.120");
		System.setProperty("http.proxyPort", "14806");
		Response res = given()
				.contentType("application/json")		
				.when()
				.cookie(Session)
				.get(url);
		return res;	
	}
}
