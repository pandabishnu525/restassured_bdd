package employeeJourney_REST_runner;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
 
@RunWith(Cucumber.class)
@CucumberOptions(features="features", glue="employeeJourney_REST_stepDefinition")
public class Runner {
	
}