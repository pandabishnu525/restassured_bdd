package employeeJourney_REST_Property;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ReadPropertiesFile {

	private Properties prop;
	public ReadPropertiesFile() {

		try {
			InputStream input = new FileInputStream("Resource/STData.properties");
			prop = new Properties();
			prop.load(input);
			prop.getProperty("AccountBCNumber");
			} catch (FileNotFoundException e) {
			e.printStackTrace();
			} catch (IOException e) {
			e.printStackTrace();
			}
	}
	public String URL_GET_PARTY_DETAIL() {
		return prop.getProperty("URL_GET_PARTY_DETAIL");
	}
	public String URL_GETOwner() {
		return prop.getProperty("URL_GETOwner");
	}	
	public String getOwner() {
		return prop.getProperty("Owner");
	}
	public String getCookie() {
		return prop.getProperty("Cookie");
	}
	public String CustomerAgreement_BCNumber() {
		return prop.getProperty("CustomerAgreement_BCNumber");
	}
	public String Invalid_AccBC() {
		return prop.getProperty("Invalid_AccBC");
	}
	public String ErrorCode_Missing() {
		return prop.getProperty("ErrorCode_Missing");
	}
	public String ErrorMessage_Missing() {
		return prop.getProperty("ErrorMessage_Missing");
	}
}
